document.addEventListener('DOMContentLoaded', function(){
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems); 
});

document.addEventListener('DOMContentLoaded', function(){
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems); 
});

var emailUsuario = document.querySelector('#emailUsuario');
var msgPrincipal = document.querySelector('#msgPrincipal');

firebase.auth().onAuthStateChanged(function(user){
    if(user){
        emailUsuario.textContent = user.email;
        msgPrincipal.textContent = 'Bem vindo ao VLanches - Admin';
    }
    else{
        alert('Você está desconectado!');
        msgPrincipal.textContent = 'Você não está conectado ao sistema';
    }
})