var comida = document.querySelector('#comida');
var preco = document.querySelector('#preco');
var ingrediente = document.querySelector('#ingredientes');
var btnImage = document.querySelector('#imgComida');
var imgUrl;
btnImage.addEventListener('change', function(e){
    var arquivo = e.target.files[0];
    var storageRef = firebase.storage().ref().child('comidas/'+ arquivo.name);
    var uploadTask = storageRef.put(arquivo);
    var imgRef = storageRef;

    uploadTask.on('state_changed', function(snapshot){
        var progresso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        if(progresso == 100)
        {
            alert('arquivo enviado');
        }
    },
    function err(err){
        console.log(err);
    },

    function complete(){
        imgRef.getDownloadURL().then(function(url){
            imgUrl = url;
        });
    }
);
});

var addComida = document.querySelector('#addComida');
addComida.addEventListener('click', function(){
    event.preventDefault();
    cadastrar(comida.value, preco.value, ingrediente.value, imgUrl);
});

function cadastrar(comida, preco, ingredientes, imgUrl)
{
    var dados = {
        nome: comida,
        preco: preco,
        ingredientes: ingredientes,
        imgUrl: imgUrl
    }
    return firebase.database().ref().child('comidas').push(dados);
}
