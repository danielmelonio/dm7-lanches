var botao = document.querySelector('#botaoEntrar');
var emailDigitado = document.querySelector('#email');
var senhaDigitada = document.querySelector('#senha');

botao.addEventListener('click', function(){
    firebase
    .auth()
    .signInWithEmailAndPassword(emailDigitado.value, senhaDigitada.value)
    .then(function(){
        window.location.href = 'index.html';
    })
    .catch(function(error){
        console.log(error.code);
        console.log(error.message);
        alert("Falha ao autenticar, verifique o erro no console!")
    });
});

firebase.auth().onAuthStateChanged(function(user){
    if(user)
        alert('Você está conectado com: ' + user.email)
    else {
        alert('Você está disconectado.');
    }
})
